<?php
//require pthread extension on php
//find odd number with concurrency 
class Odd extends Thread
{
    private $tID;
 
    public function __construct($tID)
    {
        $this->tID = $tID;
    }
 
    public function run()
    {
    	echo $this->tID . " started. ". date('H:i:s');
    	echo $this->tID . " ended. " . date('H:i:s') . "<br>";
    }
}
 

$threads = [];

for ($i = 1; $i <= 2000; $i++) {
	if($i%2 == 1)
	{
    	$threads[$i] = new Odd($i);
    	$threads[$i]->start();  
	}
}

 