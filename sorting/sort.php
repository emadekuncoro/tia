<?php 

class Sort{

	private $arr;

	function __construct($input)
	{
		$this->arr = $input;
	}

	function selection_sort() 
	{
		$array 	= $this->arr;
		$length = count($array);
		for ($j = 0; $j < $length-1; $j++) 
		{
			$iMin = $j;
			for ($i = $j+1; $i < $length; $i++) 
			{
				if ($array[$i] < $array[$iMin]) 
				{
					$iMin = $i;
				}
			}
			if ($iMin != $j) 
			{
				// swap
				$temp = $array[$j];
				$array[$j] = $array[$iMin];
				$array[$iMin] = $temp;
			}
		}

		return $array;
	}


	function bubble_sort() 
	{
		$array 	= $this->arr;
		$length = count($array);
		do {
			$sorted = true;
			for ($i = 0; $i < $length - 1; $i++) {
				if ($array[$i] > $array[$i+1]) {
					// swap
					$tmp = $array[$i];
					$array[$i] = $array[$i+1];
					$array[$i+1] = $tmp;
					$sorted = false;
				}
			}
		} while (!$sorted);
		return $array;
	}


	function insertion_sort() 
	{
		$array 	= $this->arr;
		$length = count($array);
		for ($i = 1; $i < $length; $i++) {
			$j = $i;
			while ($j > 0 && $array[$j-1] > $array[$j]) {
				// swap
				$temp = $array[$j-1];
				$array[$j-1] = $array[$j];
				$array[$j] = $temp;
				$j--;
			}
		}
		return $array;
	}
}
