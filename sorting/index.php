<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sort with PHP</title>
</head>
<body>
<form action="" method="POST">
	<input type="number" name="int" placeholder="input number">
	<input type="submit" value="Submit">
</form>	
</body>
</html>

<?php 
session_start();

require_once('sort.php');

if($_POST)
{

	$int = $_POST['int'];
	if($int%2 == 1)
	{
		if(isset($_SESSION['arr']))
		{
			$arr = $_SESSION['arr'];
		}
		else
		{
			$arr = [];
		}

		$arr[] = $int;

		$_SESSION['arr'] = $arr;


		$total = count($arr);
		if($total < 20)
		{
			$space = 20 - $total;
			echo implode(',', $arr);
			echo '<br> input '.$space.' number again !!';
		}
		elseif($total == 20)
		{
			echo 'ouput : '.implode(',', $arr).'<br>';
			echo 'sorting.. <br>';

			$sort 			= new Sort($arr);
			//selection sort
			$start_date 	= new DateTime( date('Y-m-d H:i:s') );
			$selection 		= $sort->selection_sort();
			echo 'selection sort : '.implode(',', $selection).'<br>';
			$end_date 		= new DateTime( date('Y-m-d H:i:s') );
			$elapsed_time 	= $start_date->diff( $end_date );
			echo 'total time : ';
			echo $elapsed_time->h.' hours : ';
			echo $elapsed_time->i.' minutes : ';
			echo $elapsed_time->s.' seconds<br>';
			//bubble sort
			$start_date 	= new DateTime( date('Y-m-d H:i:s') );
			$bubble 		= $sort->bubble_sort();
			echo 'bubble sort : '.implode(',', $bubble).'<br>';
			$end_date 		= new DateTime( date('Y-m-d H:i:s') );
			$elapsed_time 	= $start_date->diff( $end_date );
			echo 'total time : ';
			echo $elapsed_time->h.' hours : ';
			echo $elapsed_time->i.' minutes : ';
			echo $elapsed_time->s.' seconds<br>';
			//insertion sort
			$start_date 	= new DateTime( date('Y-m-d H:i:s') );
			$insertion 		= $sort->insertion_sort();
			echo 'insertion sort : '.implode(',', $insertion).'<br>';
			$end_date 		= new DateTime( date('Y-m-d H:i:s') );
			$elapsed_time 	= $start_date->diff( $end_date );
			echo 'total time : ';
			echo $elapsed_time->h.' hours : ';
			echo $elapsed_time->i.' minutes : ';
			echo $elapsed_time->s.' seconds<br>';

			session_destroy();
		}
		else
		{
			session_destroy();
		}
	}
	else
	{
		echo 'Oops, only accept odd numbers <br>';
	}
}

 ?>