<?php 

//no concurrency
$start_date = new DateTime( date('Y-m-d H:i:s') );
echo 'start time : '. date('Y-m-d H:i:s');
echo '<br>';
for($i=1;$i<=2000;$i++)
{
    if($i%2 == 1)
    {
        echo 'Process '.$i.' started in '.date('H:i:s').'<br>';
    }
}
$end_date = new DateTime( date('Y-m-d H:i:s') );
echo 'end time : '.date('Y-m-d H:i:s');
echo '<br> total time : ';
$elapsed_time = $start_date->diff( $end_date );
echo $elapsed_time->h.' hours : ';
echo $elapsed_time->i.' minutes : ';
echo $elapsed_time->s.' seconds<br>';